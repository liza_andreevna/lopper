$(function () {
    //slider in shaping section
    let slides = $('.mySlides');
    let buttonPrev = $('.button__prev');
    let buttonNext = $('.button__next');
    let slideIndex = 1;
    let autoPlay = true;

    showSlides(slideIndex);

    setInterval(function () {
        if (autoPlay == true) showSlides(slideIndex += 1);
    }, 3000);

    $('.slider-control').hover(
        function () {
            autoPlay = false;
        },
        function () {
            autoPlay = true;
        }
    );

    buttonPrev.click(function () {
        showSlides(slideIndex += -1);
    });

    buttonNext.click(function () {
        showSlides(slideIndex += 1);
    });

    function showSlides(n) {
        let i;
        if (n > slides.length) slideIndex = 1;
        if (n < 1) slideIndex = slides.length;
        for (i = 0; i < slides.length; i++) {
            slides.eq(i).hide();
        }
        slides.eq(slideIndex - 1).show();
        $('.slider-control__number').html(writeCurrentSlide());
    }

    function writeCurrentSlide() {
        return `${slideIndex}  /  ${slides.length}`;
    }


    $('.burger').on("click", function (e) {
        e.preventDefault();
        if ($(window).width() < 769) {
            if ($(this).hasClass('active')) {
                closeMenu();
            } else {
                openMenu();
            }
        } else return false;
    })

    function openMenu() {
        $('.burger').toggleClass('active');
        $('.mobile-menu').css({
            'width': '100%'
        })
        $('.navbar__burger').css({
            'opacity': '0'
        })
    }

    function closeMenu() {
        $('.burger').toggleClass('active');
        $('.mobile-menu').css({
            'width': '0%'
        });
        $('.navbar__burger').css({
            'opacity': '1'
        })
    }
})